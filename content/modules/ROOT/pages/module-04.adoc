= Advanced Inventories

In Ansible and automation controller, as you know, everything starts with an inventory. There are several methods how inventories can be created. Starting from simple static definitions over importing inventory files to dynamic and smart inventories.

In real life it’s very common to deal with external dynamic inventory sources (think cloud, CMDB, containers, ...). In this chapter we’ll introduce you to building dynamic inventories using custom inventory scripts. Another great feature of the automation controller to deal with inventories is the Constructed Inventory feature, which you’ll do a lab on as well.

== Dynamic Inventories

Automation controller includes built-in support for syncing dynamic inventories from cloud sources such as Amazon AWS, Google Compute Engine, and others.

* Inventory plugins are the newer way to generate custom inventories and must be written in Python, but inventory scripts still work.
* If the source you want to query is not covered by an existing inventory plugin, you can create your own script or plugin.

In this chapter you’ll get started with custom dynamic inventory _scripts_.
Among all the available programming languages we have chosen one of the most accessible and known: Bash! Yes!

IMPORTANT: Don’t get this wrong... we’ve chosen to use Bash to make it as simple as possible to show the concepts behind dynamic and custom inventories.
Our recommendation in real life remains to use Python to write your dynamic inventory _plugins_ (not scripts!), and profit from libraries, documentation and examples written in the same language.
And actually you should even place the inventory plugin in a collection and use it as part of this collection added to an Execution Environment loaded into the controller.
But this approach would have been too involved for this lab, so here you are: Bash!

=== The Inventory Source

First you need an inventory source. In *real life* this would be your cloud provider, your CMDB, or what not. For the sake of this lab we put a simple file into a GitLab repository.

Use curl to query your "external inventory source":

[source,shell,role=execute,subs="attributes"]
----
curl {gitea_web_ui_url}/lab-user/playbooks-adv-controller/raw/branch/main/constructed-inventory/inventory_list
----

This should give you the following output:

[source,json]
----
{
    "dyngroup":{
        "hosts":[
            "cloud1.cloud.example.com",
            "cloud2.cloud.example.com",
            "CLOUD3.cloud.example.com"
        ],
        "vars":{
            "var1": true
        }
    },
    "all":{
        "hosts":[
            "cloud4.prod.cloud.example.com",
            "cloud5.prod.cloud.example.com",
            "cloud6.dev.cloud.example.com",
            "cloud7.dev.cloud.example.com"
        ]
    },
    "_meta":{
        "hostvars":{
            "cloud1.cloud.example.com":{
                "type":"web",
                "architecture":"x86_64"
            },
            "cloud2.cloud.example.com":{
                "type":"database",
                "architecture":"arm"
            },
            "cloud3.cloud.example.com":{
                "type":"database",
                "architecture":"x86_64"
            },
            "cloud4.prod.cloud.example.com":{
                "type":"web",
                "architecture":"arm"
            },
            "cloud5.prod.cloud.example.com":{
                "type":"web",
                "architecture":"x86_64"
            },
            "cloud6.dev.cloud.example.com":{
                "type":"web",
                "architecture":"arm"
            },
            "cloud7.dev.cloud.example.com":{
                "type":"web",
                "architecture":"x86_64"
            }
        }
    }
}
----

Well, this is handy, the output is already presented as JSON, as expected by Ansible... ;-)

NOTE: Okay, seriously, in real life your plugin would likely get some information from your source system, format it as JSON and return the data to the automation controller.

==== The Custom Inventory Script

An inventory script (and plugins, too) has to follow some conventions. It must accept the *`--list`* and *`--host <hostname>`* arguments. When it is called with *`--list`*, the script must output JSON-encoded data containing all groups and hosts to be managed. When called with *`--host <hostname>`* it must return an JSON-formatted hash or dictionary of host variables (can be empty) for this particular host.

As looping over all hosts and calling the script with *`--host`* can be pretty slow, it is possible to return a top level element called `\_meta` with all host variables together with the output of *`--list`* in one run. And this is what we’ll do. So this is our custom inventory script:

[source,bash,role=execute,subs="attributes"]
----
#!/bin/bash

if [ "$1" == "--list" ] ; then
    curl -sS {gitea_web_ui_url}/lab-user/playbooks-adv-controller/raw/branch/main/constructed-inventory/inventory_list
elif [ "$1" == "--host" ]; then
    echo '{"_meta": {"hostvars": {}}}'
else
    echo "{ }"
fi
----

What it basically does is to return the data collected by curl when called with *`--list`* and as the data includes *\_meta* information about the host variables Ansible will not call it again with *`--host`*. The curl command is of course the place where your plugin would get data by whatever means, format it as proper JSON and return it (`-sS` makes curl silent, except for error messages).

But before we integrate the custom inventory plugin into our automation controller, it’s a good idea to test it on the command line:

* Bring up your VS Code browser tab, refer to the xref:lab-access.adoc[Lab Access] page if the tab was closed.
* In either the visual editor or in the terminal using your favorite command line editor, create the file `/home/{bastion_ssh_user_name}/dyninv.sh` with the content of the Bash script shown above.

TIP: Make sure to really create the script in `/home/{bastion_ssh_user_name}/`

* Make the script executable:
+
[source,shell,role=execute]
----
chmod +x dyninv.sh
----

* Execute it:
+
[source,shell,role=execute]
----
./dyninv.sh --list
----

This should give you the same output as above.

As simple as it gets, right? More information can be found on https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html[how to develop dynamic inventories,window=_blank].

So, now you have a source of a (slightly static) dynamic inventory (talk about an oxymoron…) and a script to fetch and pass it to the automation controller.

=== Integrate into automation controller

In Ansible Tower up to version 3.8, you could create inventory scripts directly in the web UI. Since automation controller 4.0 the only way to get inventory scripts into automation controller is by putting the script into a source control repository.

For this lab the inventory script was already created in the Git repository you have configured as a *Project* earlier, so you can use this as-is.

You can directly proceed to adding the dynamic inventory and pointing it to the inventory script.

* In the web UI, open *Inventories* in the *Resources* section.
* To create a new custom inventory, click the blue Add button and click on *Add inventory*.
* Fill in the needed data:
  ** *Name:* Cloud inventory
* Click *Save*
* Change to the *Sources* tab and once more click the blue add button.
* Fill in the needed data:
  ** *Name:* Cloud inventory Script
  ** *Source:* Sourced from a Project
  ** *Project:* Lab Project Repo
  ** *Inventory file:* constructed-inventory/inventory-script
  ** enable *Update on launch*
* Click on *Save*
* Start the initial sync by clicking on *Sync*

Navigate to *Jobs* in the *Views* section to watch the initial sync, the *Type* is `Inventory Sync`.

After the inventory sync has finished check out the new hosts which were added by it to your _Cloud inventory_, by navigating to *Hosts* in the *Resources* section. You should find a number of new hosts according to the JSON output above.

=== What is the take-away?

Using this simple example you have:

* Created a script to query an inventory source
* Integrated the script into controller
* Populated an inventory using the custom script

== Constructed Inventories

You might remember the concept of Smart Inventories. They allowed us to create inventories over multiple inventory sources and optionally combine them with some built-in filters. However, smart inventories also had some severe limitations, most notably, smart inventories lose host group membership information.

Therefore, the concept of smart inventories, although still available in automation controller, is deprecated and replaced by constructed inventories. They are far more flexible and of course, they maintain the group membership information of your hosts.

=== Introduction

Let's create our first constructed inventory by navigating to *Inventories* in the *Resources* section in the main menu on the left.

* Create a new inventory by clicking on the blue Add button and then *Add constructed inventory*.
* Use "Constructed Inventory" as its name
* Click on the magnifying glass next to *Input Inventories* and select the existing `Cloud inventory` and `Lab inventory`. Click *Select*
* In the input field *Source vars* add the following details:

[source,yaml]
----
---
plugin: ansible.builtin.constructed
strict: false
----

The first line informs the automation controller that we want to use the constructed inventory plugin. The *strict* setting set to "false" allows us to use variables later, which don't have to be defined. With *strict* set to "true" automation controller will fail to parse the constructed inventory, if a variable is not defined for any particular host.

* Click **Save**

Now while in the details view of the `Constructed Inventory`:

*  Hit the sync button.
* After the sync has finished, have a look at the **Hosts** tab
* You should see all hosts from the `Cloud inventory` and the `Lab inventory` combined.
* You should also see a group called *dyngroup* which was imported from our dynamic inventory `Cloud inventory`.

=== Compose

In historically grown environments you often have the problem that your inventory source has some legacy information, which you want to beautify or have to fix so Ansible can actually reach the remote host. In our first example, let's assume you have an inventory source which returns some hostnames with upper case characters. To make everything work smoothly, we want all fully qualified domain names to be converted to lower case characters.

Let's expand our configuration to the following, edit again the **Source vars** of the `Constructed Inventory` to look as follows:

[source,yaml]
----
---
plugin: ansible.builtin.constructed
strict: false
compose:
  ansible_host: inventory_hostname | lower()
----

When connecting to a remote host Ansible is using the *ansible_host* variable - this is different from the *inventory_hostname* which is what we for example see in the UI when we click on *Hosts*. Click *Save* and then *Sync* to apply the constructed inventory.

After the sync completed, check the tabs *Hosts* and *Groups*. You should see a host `CLOUD3.cloud.example.com` and if you click on it, you will see it has a variable *ansible_host* with the FQDN converted to lower case characters. 

=== Groups

Many organizations use a strict convention for hostnames. Often a hostname indicates the purpose and the stage of a given system. In our case we have a couple of hosts which are in the *prod* and in the *dev* sub domain. We can use this information to automatically add the hosts to the respective group. Let's extend our constructed inventory configuration.

Navigate back to your constructed inventory by clicking on *Inventories* in the *Resources* section of the main menu and then edit the `Constructed Inventory` by clicking on the pen icon on the right hand side.

Change the input field *Source vars* to the following:

[source,yaml]
----
---
plugin: ansible.builtin.constructed
strict: false
compose:
  ansible_host: inventory_hostname | lower()
groups:
  production: inventory_hostname is search('prod')
  development: inventory_hostname is search('dev')
----

Click on *Save* and *Sync* again. When you navigate to the *Hosts* tab of the `Constructed Inventory`, you should not notice any difference. However, in the *Groups* tab you should see two new groups and you should find the respective hosts in the production and development group.

=== Keyed Groups

In this last example we want to use host variables to automatically add systems to specific groups. Instead of hard coding the group names, we want to use the value of the variables as group names. 

Let's assume your systems have a variable `architecture` with the CPU architecture (like ARM and x86_64) and a variable `type` showing the role of the system (web or database). Instead of manually assigning these groups, we can use the value of these variables. An added benefit of this approach is, if you ever introduce new CPU architectures or roles, the groups will automatically update accordingly.

As an example here are the variables assigned to host `cloud1.cloud.example.com`:

[source,yaml]
----
{
  "ansible_host": "cloud1.cloud.example.com",
  "architecture": "x86_64",
  "type": "web"
}
----

As before, change the settings for your constructed inventory. You should know by now, how to get to the menu and apply the change:

[source,yaml]
----
---
plugin: ansible.builtin.constructed
strict: false
compose:
  ansible_host: inventory_hostname | lower()
  name: inventory_hostname | lower()
groups:
  production: inventory_hostname is search('prod')
  development: inventory_hostname is search('dev')
keyed_groups:
  - prefix: ""
    separator: ""
    key: architecture
  - prefix: "server_type"
    separator: "-"
    key: type
----

After you have saved and synced the changes have a look at the new groups that were created:

* You should see new groups per CPU architecture with the respective hosts showing up. The group name is taken from the `architecture` variable of the hosts.
* The second keyed groups entry is creating groups according to the `type` variable of the host. This time the group name is prefixed with the string `server_type` and a seperator.

If a host has multiple matching keys (like `database` and `web`), the separator is used to separate them in the group name. In our example, each host is in only one group so the separator is only seen in front of the `type`.

TIP: Have you noticed what happened to the separator? The dash ("**-**") automatically became an underscore ("**_**").
Dashes used to be allowed in group names but not anymore.
Name your groups (and roles and playbooks, as part of collections) like Python variables, with only letters, digits and underscores.

If you want to learn more about constructed inventories, there is a great https://www.redhat.com/en/blog/how-to-use-the-new-constructed-inventory-feature-in-aap-2.4[Blog Post,window=_blank] and of course the https://docs.ansible.com/automation-controller/latest/html/userguide/inventories.html#ug-inventories-constructed[automation controller documentation,window=_blank].
